/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    
    public void TopFiveProactiveUsersBasedOnAll(){
        
    Map<Integer,Post> posts = DataStore.getInstance().getPosts();
    Map<Integer,User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> userall= new HashMap<>();
        
        
        for(User u:users.values()){
            int count =0;
            count+= u.getComments().size();
            for(Comment c:u.getComments()){
                
                if(userall.containsKey(u.getId())){
                    count = userall.get(u.getId());
                }
                count +=c.getLikes();
            }
            
            for(Post p:posts.values()){
            if(u.getId()==p.getPostId()){
                count += 1;  
                }
            }
            userall.put(u.getId(), count);
        }
        
        List<Map.Entry<Integer, Integer>> useralllist = new ArrayList<Map.Entry<Integer, Integer>>(userall.entrySet());
        
        Collections.sort(useralllist,new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Entry<Integer, Integer> o1,
                    Entry<Integer, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });
        
        System.out.println("The id of top 5 proactive users overall (comments, posts and likes) are:");
        int n=useralllist.size()-1;
        for(int i=0;i<5;i++){
            System.out.println(useralllist.get(i).getKey());
        }
    }
    
    public void TopFiveInactiveUsersBasedOnAll(){
        
    Map<Integer,Post> posts = DataStore.getInstance().getPosts();
    Map<Integer,User> users = DataStore.getInstance().getUsers();
        Map<Integer, Integer> userall= new HashMap<>();
        
        
        for(User u:users.values()){
            int count =0;
            count+= u.getComments().size();
            for(Comment c:u.getComments()){
                
                if(userall.containsKey(u.getId())){
                    count = userall.get(u.getId());
                }
                count +=c.getLikes();
            }
            
            for(Post p:posts.values()){
            if(u.getId()==p.getPostId()){
                count += 1;  
                }
            }
            userall.put(u.getId(), count);
        }
        
        List<Map.Entry<Integer, Integer>> useralllist = new ArrayList<Map.Entry<Integer, Integer>>(userall.entrySet());
        
        Collections.sort(useralllist,new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Entry<Integer, Integer> o1,
                    Entry<Integer, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });
        
        System.out.println("The id of top 5 inactive users overall (comments, posts and likes) are:");
        int n=useralllist.size()-1;
        for(int i=n;i>=n-4;i--){
            System.out.println(useralllist.get(i).getKey());
        }
    }
    
    public void userWithMostLikes(){
        Map<Integer,Integer> userLikeCount = new HashMap<>();
        Map<Integer,User> users = DataStore.getInstance().getUsers();
        for(User user:users.values()){
            for(Comment c:user.getComments()){
                int likes = 0;
                if(userLikeCount.containsKey(user.getId())){
                    likes = userLikeCount.get(user.getId());
                }
                likes += c.getLikes();
                userLikeCount.put(user.getId(), likes);
            }
        }
        int max = 0;
        int maxId = 0;
        for(int id:userLikeCount.keySet()){
            if(userLikeCount.get(id) > max){
                max = userLikeCount.get(id);
                maxId = id;
            }
        }
        System.out.println("User with most Likes is:" + max +"\n" + users.get(maxId));
    }
    
    public void getFiveMostLikedComment(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentList = new ArrayList<>(comments.values());
        Collections.sort(commentList, new Comparator<Comment>(){
            @Override
            public int compare(Comment c1, Comment c2){
                return c2.getLikes() - c1.getLikes();
            }
        });
        
        System.out.println("5 most liked comments:");
        for(int i=0;i<5;i++){
            System.out.println(commentList.get(i));
        }
    }
    
    public void averageLikesPerComment(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        int likesum = 0;
        for(int i=0;i<comments.size();i++){
            likesum += comments.get(i).getLikes();
        }
        System.out.println("Average Likes per comment:" + likesum/comments.size());
    }
    
    public void postWithMostLikedComments(){
        Map<Integer,Post> posts = DataStore.getInstance().getPosts();
        Map<Integer,Integer> postLikeCount = new HashMap<>();
        for(Post p:posts.values()){
            for(Comment c:p.getComments()){
                int likes = 0;
                if(postLikeCount.containsKey(p.getPostId())){
                    likes = postLikeCount.get(p.getPostId());
                }
                likes += c.getLikes();
                postLikeCount.put(p.getPostId(), likes);
            }
        }
        int max = 0;
        int maxId = 0;
        for(int id:postLikeCount.keySet()){
            if(postLikeCount.get(id)>max){
                max = postLikeCount.get(id);
                maxId = id;
            }
        }
        System.out.println("Post with most Likes is:" + max +"\n" + posts.get(maxId));
    }
    
    public void postWithMostComments(){
        Map<Integer,Post> posts = DataStore.getInstance().getPosts();
        int max = 0;
        int maxId = 0;
        for(Post p:posts.values()){
            if(p.getComments().size()>max){
                max = p.getComments().size();
                maxId = p.getPostId();
            }
        }
        System.out.println("Post with most commments is:" + max + "\n" + posts.get(maxId));
    }
       public static HashMap sortByValues(HashMap map) {
        List list = new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });
        HashMap sortedHashMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }

        return sortedHashMap;
    }
public void topFiveInactiveUserBasedOnPosts() {
        HashMap<Integer, Integer> user = new HashMap<Integer, Integer>();
        Map<Integer, Post> post = DataStore.getInstance().getPosts();
        List<Post> postLis = new ArrayList<Post>(post.values());
        for (Post posts : postLis) 
        {
            int count = 0;
            if (user.containsKey(posts.getUserId())) 
            {
                count = user.get(posts.getUserId());
            }
            count++;
            user.put(posts.getUserId(), count);
        }
        Map<Integer, String> ma = sortByValues(user);
        System.out.println("The top 5 inactive users based on posts are:");
        Set s = ma.entrySet();
        int i = 0;
        Iterator it = s.iterator();
        while (it.hasNext()) {
            if (i< 5) 
            {
                Map.Entry m = (Map.Entry) it.next();
                System.out.println("User ID " + m.getKey()+ " posts "+m.getValue());
                i++;
            }
            else
                break;
        }
    }
    public void topFiveInactiveUserBasedOnComments() {
        HashMap<Integer, Integer> user = new HashMap<Integer, Integer>();
        Map<Integer, Comment> comment = DataStore.getInstance().getComments();
        List<Comment> com = new ArrayList<Comment>(comment.values());
        for (Comment comments : com) {
            int count = 0;
            if (user.containsKey(comments.getUserId())) 
            {
                count = user.get(comments.getUserId());
            }
            count++;
            user.put(comments.getUserId(), count);
        }
        Map<Integer, String> map = sortByValues(user);
        System.out.println("The top 5 inactive users based on comments are:");
        Set s = map.entrySet();
        int i= 0;
        Iterator it = s.iterator();
        while (it.hasNext()) {
            if (i< 5) 
            {
                Map.Entry m = (Map.Entry) it.next();
                System.out.println("User ID  " + m.getKey()+" Comments "+m.getValue());
                i++;
            }
            else
                break;
        }
    }

  
}

